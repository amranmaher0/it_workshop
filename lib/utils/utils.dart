import 'package:flutter/cupertino.dart';

import '../common/enums.dart';

class Utils {
  static String? requestTypeToString(RequestType requestType) {
    String? type = '';
    switch (requestType) {
      case RequestType.get:
        {
          type = 'GET';
        }
        break;
      case RequestType.post:
        {
          type = 'POST';
        }
        break;
      case RequestType.delete:
        {
          type = 'DELETE';
        }
        break;
    }
    return type;
  }

  // ~ pickImage
  // static Future<XFile> pickImage({bool fromCamera = false}) async {

  //   XFile? image = await ImagesPicker.pick(
  //           source: fromCamera ? ImageSource.camera : ImageSource.gallery);
  //   // if (image == null) ;
  //   return image ?? XFile('');
  // }

  // ~ pickImages
  // static Future<List<XFile>> pickImages() async {
  //   ImagePicker picker = ImagePicker();
  //   FilePickerResult picker2 = FilePicker().pickFiles();
  //   List<XFile> imageFileList = [];
  //   final List<XFile>? selectedImages = await picker.pickMultiImage();
  //   if (selectedImages!= null) {
  //     imageFileList.addAll(selectedImages);
  //   }
  //   log("Image List Length:" + imageFileList.length.toString());
  //   return imageFileList;
  // }

  // static Future<> pickImages()async{
  //    final result = await FilePicker.platform.pickFiles(
  //     allowMultiple: true,
  //   );
  //   if (result == null) return;
  // }

  static bool isKeyBoardOpen() {
    if (WidgetsBinding.instance.window.viewInsets.bottom > 0.0) {
      return true;
    } else {
      return false;
    }
  }
}
