import 'package:flutter/material.dart';

import '../../../common/widgets/circular_indicator_widget.dart';
import '../../../extensions/widget_extension.dart';
import '../../../themes/app_colors.dart';
import '../../themes/text_styles.dart';

class DmTextFieldWidget extends StatelessWidget {
  const DmTextFieldWidget({
    Key? key,
    this.onSaved,
    this.validator,
    this.controller,
    required this.onChange,
    this.labelText,
    this.initialValue = '',
    this.obscureText = false,
    this.suffixIcon = false,
    this.hideLabelText = false,
    this.hideShadow = false,
    this.height,
    this.maxLine = 1,
    this.minLine = 1,
    required this.textFieldType,
    this.suffixIconOnPressed,
    this.labelColor,
    this.borderColor,
    this.fillColor,
    this.errorText,
    this.isLoading = false,
    this.focusNode,
    this.textStyle,
    this.textInputAction,
    this.onFieldSubmitted,
    this.contentPadding,
  }) : super(key: key);
  final Function(String?)? onSaved;
  final String? Function(String?)? validator;
  final String? Function(String?)? onChange;
  final String? labelText;
  final double? height;
  final int? maxLine;
  final int? minLine;
  final String initialValue;
  final Color? labelColor;
  final Color? borderColor;
  final Color? fillColor;
  final bool obscureText;
  final bool hideLabelText;
  final bool hideShadow;
  final bool suffixIcon;
  final TextInputType? textFieldType;
  final VoidCallback? suffixIconOnPressed;
  final TextEditingController? controller;
  final String? errorText;
  final bool isLoading;
  final FocusNode? focusNode;
  final TextStyle? textStyle;
  final TextInputAction? textInputAction;
  final void Function(String)? onFieldSubmitted;
  final EdgeInsetsGeometry? contentPadding;
  @override
  Widget build(BuildContext context) {
    // ~ We use the TextFormField only once in the whole application
    return Container(
      decoration: BoxDecoration(
        boxShadow: hideShadow
            ? null
            : const [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 1.0,
                  spreadRadius: 0.1,
                  offset: Offset(
                    -2.0,
                    2.0,
                  ),
                )
              ],
        color: fillColor ?? Colors.white,
      ),
      child: SizedBox(
        height: height ?? 35,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: height ?? 35,
              decoration: errorText == null
                  ? null
                  : BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.red, width: 1),
                    ),
              child: TextFormField(
                onFieldSubmitted: onFieldSubmitted,
                textInputAction: textInputAction,
                focusNode: focusNode,
                keyboardType: textFieldType,
                obscureText: obscureText,
                initialValue: initialValue,
                style: textStyle,
                maxLines: maxLine, //maxLine ?? 10,
                minLines: maxLine ?? 1, //1,
                decoration: InputDecoration(
                  contentPadding: contentPadding ??
                      const EdgeInsets.symmetric(
                        vertical: 2,
                        horizontal: 8,
                      ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                      color: borderColor ?? Colors.grey,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(
                      color: AppColors.primaryColor,
                      width: 1,
                    ),
                  ),
                  filled: true,
                  fillColor: fillColor ?? Colors.white,
                  suffixIcon: suffixIcon
                      ? IconButton(
                          onPressed: suffixIconOnPressed ?? () {},
                          icon: obscureText
                              ? const Icon(
                                  Icons.visibility_off,
                                  color: AppColors.primaryColor,
                                )
                              : const Icon(
                                  Icons.visibility,
                                  color: Colors.grey,
                                ),
                        )
                      : isLoading
                          ? const CircularProgressIndicatorWidget(
                              color: Colors.grey,
                            ).fittedBox().padding(const EdgeInsets.all(10))
                          : null,
                  labelText: errorText == null
                      ? hideLabelText
                          ? null
                          : labelText
                      : null,
                  labelStyle: h4TextStyle(color: labelColor),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: borderColor ?? Colors.grey),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: borderColor ?? Colors.transparent,
                      width: 0.5,
                    ),
                  ),
                ),
                onSaved: onSaved,
                onChanged: onChange,
                validator: validator,
              ),
            ),
            errorText == null
                ? Container()
                : Text(
                    errorText!,
                    style: h4TextStyle(
                      color: Colors.red,
                    ),
                  ).padding(const EdgeInsets.only(left: 8, right: 8, top: 2)),
          ],
        ),
      ),
    );
  }
}
