import 'package:flutter/material.dart';
import 'package:it_workshop/common/app_assets.dart';
import 'package:it_workshop/common/app_assets.dart';

class DmLogoWidget extends StatelessWidget {
  const DmLogoWidget({
    Key? key,
    this.scale,
    this.color,
  }) : super(key: key);
  final double? scale;
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppAssets.appLogo,
      color: color ?? Colors.white,
      width: scale ?? 150,
      height: scale ?? 150,
    );
  }
}
