import 'package:flutter/material.dart';

class DmDividerWidget extends StatelessWidget {
  const DmDividerWidget({Key? key, this.color, this.thickness})
      : super(key: key);
  final Color? color;
  final double? thickness;
  @override
  // ~ We use the divider widget only once in the whole application

  Widget build(BuildContext context) {
    return Divider(
      color: color ?? Colors.black38,
      thickness: thickness ?? 0.5,
    );
  }
}
