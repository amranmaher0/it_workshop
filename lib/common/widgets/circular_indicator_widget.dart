import 'package:flutter/material.dart';

class CircularProgressIndicatorWidget extends StatelessWidget {
  const CircularProgressIndicatorWidget({
    this.color = Colors.white,
    this.scale,
    Key? key,
  }) : super(key: key);
  final Color color;
  final double? scale;

  @override
  Widget build(BuildContext context) {
    // ~ We use the CircularProgressIndicator only once in the whole application
    return SizedBox(
      width: scale ?? 10,
      height: scale ?? 10,
      child: CircularProgressIndicator(
        strokeWidth: 1.5,
        valueColor: AlwaysStoppedAnimation<Color>(color),
      ),
    );
  }
}
