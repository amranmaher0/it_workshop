import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../extensions/widget_extension.dart';
import '../../themes/app_colors.dart';
import '../../themes/text_styles.dart';

class DmAppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  const DmAppBarWidget({
    Key? key,
    this.title,
    this.onPrefixTapped,
    this.onSuffixTapped,
    this.prefix,
    this.suffix,
    this.leading,
    this.rightWidget,
    this.appBarColor,
    this.titleColor,
    this.backButtonColor,
    this.haveBackButton = false,
    this.transparentAppBar = false,
  }) : super(key: key);
  final Widget? prefix;
  final Widget? suffix;
  final Widget? leading;
  final Widget? rightWidget;
  final VoidCallback? onPrefixTapped;
  final VoidCallback? onSuffixTapped;
  final Color? appBarColor;
  final Color? titleColor;
  final Color? backButtonColor;
  final String? title;
  final bool haveBackButton;
  final bool transparentAppBar;

  @override
  Size get preferredSize => const Size.fromHeight(100);

  @override
  Widget build(BuildContext context) {
    return Container(
      // ~ We use the Appbar only once in the whole application
      // @ default app bar height is 56
      height: 56,
      color: transparentAppBar
          ? appBarColor ?? Colors.transparent
          : AppColors.primaryColor,
      child: Row(
        children: [
          leading ?? Container(),
          haveBackButton
              ? Container(
                  color: Colors.transparent,
                  height: Get.height,
                  width: Get.width,
                  padding: const EdgeInsets.all(20),
                  child: const Icon(Icons.arrow_back),
                ).onTap(Get.back).expanded(flex: 3)
              : Container().expanded(flex: 3),
          SizedBox(
            child: Text(
              title ?? '',
              style: h3TextStyle(
                color: titleColor ?? Colors.white,
                isBold: true,
              ),
            ).center(),
          ).expanded(flex: 8),
          Expanded(
            flex: 3,
            child: rightWidget ??
                SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      suffix?.onTap(onSuffixTapped ?? () {}) ?? Container(),
                      prefix?.onTap(onPrefixTapped ?? () {}) ?? Container(),
                    ],
                  ),
                ),
          ),
        ],
      ),
    );
  }
}
