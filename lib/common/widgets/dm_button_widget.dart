import 'package:flutter/material.dart';

import '../../common/widgets/circular_indicator_widget.dart';
import '../../extensions/widget_extension.dart';
import '../../themes/app_colors.dart';
import '../../themes/text_styles.dart';

class DmButtonWidget extends StatelessWidget {
  const DmButtonWidget({
    Key? key,
    required this.title,
    this.onTap,
    this.buttonColor,
    this.textColor,
    this.buttonHeight,
    this.buttonWidth,
    this.isLoading = false,
    this.shouldReload = false,
    this.isChecked,
    this.borderRadius,
    this.borderColor,
  }) : super(key: key);
  final String title;
  final Color? buttonColor;
  final Color? textColor;
  final double? buttonHeight;
  final double? buttonWidth;
  final VoidCallback? onTap;
  final bool shouldReload;
  final bool isLoading;
  final bool? isChecked;
  final BorderRadius? borderRadius;
  final Color? borderColor;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (isLoading) return;
        onTap?.call();
      },
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: buttonHeight ?? 25,
            width: buttonWidth ?? double.infinity,
            decoration: BoxDecoration(
              borderRadius:
                  borderRadius ?? const BorderRadius.all(Radius.circular(20)),
              color: isChecked ?? true
                  ? buttonColor ?? AppColors.primaryColor
                  : null,
              border: isChecked ?? true
                  ? null
                  : Border.all(
                      color: borderColor ?? AppColors.primaryColor, width: 1),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    title,
                    style: h4TextStyle(
                      color: isChecked ?? true
                          ? textColor ?? Colors.white
                          : AppColors.primaryColor,
                      isBold: true,
                    ),
                  ).center(),
                ),
                const SizedBox(
                  width: 5,
                ),
                isLoading
                    ? const Center(child: CircularProgressIndicatorWidget())
                    : Container()
              ],
            ),
          ),
        ],
      ),
    );
  }
}
