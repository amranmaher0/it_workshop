class StatusModel {
 String status;
 String message;

 StatusModel({
 required this.status,
 required this.message,
 });

 factory StatusModel.fromJson(Map<String, dynamic> json) => StatusModel(
 status: json["status"],
 message: json["message"],
 );
 factory StatusModel.zero() => StatusModel(
 status: '',
 message: '',
 );
}

