import 'package:it_workshop/common/enums.dart';

class GenericException implements Exception {
  ExceptionType type;
  String errorMessage;
  GenericException({required this.type, this.errorMessage = "Unknown Error"});

  @override
  String toString() {
    return errorMessage;
  }
}

Map<String, GenericException> badRequestException = {
  "PASSWORD_INVALID": GenericException(
    type: ExceptionType.PasswordInvalid,
    errorMessage: "invalid_password",
  ),
  "INVALID_CREDENTIALS": GenericException(
    type: ExceptionType.InvalidCredentials,
    errorMessage: "invalid_credentials",
  ),

  ///
  "EMAIL_ALREADY_EXISTS": GenericException(
    type: ExceptionType.EmailAlreadyExists,
    errorMessage: "email_already_exists",
  ),
  "USERNAME_ALREADY_EXISTS": GenericException(
    type: ExceptionType.UserNameAlreadyExists,
    errorMessage: "username_already_exists",
  ),
};

Map<int, GenericException> statusCodesException = {
  401: GenericException(
    type: ExceptionType.NotAuthorized,
    errorMessage: "you_are_not_authorized",
  ),
  404: GenericException(
    type: ExceptionType.NotFound,
    errorMessage: "page_not_found",
  ),
  410: GenericException(
    type: ExceptionType.PageGone,
    errorMessage: "page_gone",
  ),
  500: GenericException(
    type: ExceptionType.InternalServerException,
    errorMessage: "server_down",
  ),
  503: GenericException(
    type: ExceptionType.ServiceUnavailableException,
    errorMessage: "service_unavailable",
  ),
};
