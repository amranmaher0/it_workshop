import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class Toast {
  static void show({
    required String? message,
    required Color? backgroundColor,
    required Color? textColor,
  }) =>
      Fluttertoast.showToast(
        msg: message ?? '',
        gravity: ToastGravity.SNACKBAR,
        timeInSecForIosWeb: 1,
        backgroundColor: backgroundColor,
        textColor: textColor,
        fontSize: 16.0,
      );

  static void error(String? message) => show(
        message: message,
        backgroundColor: Get.theme.errorColor,
        textColor: Colors.white,
      );

  static void success(String? message) => show(
        message: message,
        backgroundColor: Get.theme.dialogBackgroundColor,
        textColor: Colors.black,
      );
}
