enum RequestType { get, post, delete }

enum ExceptionType {
  ConnectionError,
  // related to http status code exceptions
  NotAuthorized,
  NotFound,
  InternalServerException,
  ServiceUnavailableException,
  PageGone,

  // related to bad request status code
  // related to auth requests
  EmailAlreadyExists,
  UserNameAlreadyExists,
  PasswordInvalid,
  InvalidCredentials,

  // other
  Other,
}
