import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Dialogs {
  static Future<void> show(
    Widget widget, {
    double? dialogHeight,
    double? dialogWidth,
    double? dialogPadding,
    Color? dialogColor,
  }) {
    // ~ main Function : we call Get.dialog() one at a time only
    return Get.dialog(
      Dialog(
        child: Padding(
          padding: EdgeInsets.all(dialogPadding ?? 8.0),
          child: Container(
            width: dialogWidth ?? double.infinity,
            height: dialogHeight ?? (Get.mediaQuery.size.height) / 5,
            color: dialogColor ?? Colors.white,
            child: widget,
          ),
        ),
      ),
    );
  }

  static Future<void> warning({
    required String message,
    required VoidCallback? onFirstButtonTap,
    required VoidCallback? onSecondButtonOnTap,
    String? secondButtonTitle,
    String? firstButtonTitle,
    Color? iconColor,
    Color? messageColor,
  }) {
    return show(
      Container(),
    );
  }

  static Future<void> message({
    required String message,
    required VoidCallback onButtonTapped,
    String? iconPath,
    String? buttonTitle,
    Color? buttonColor,
    Color? iconColor,
    Color? buttonTitleColor,
  }) {
    return show(Container());
  }
}
