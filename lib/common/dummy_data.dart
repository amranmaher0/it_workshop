const Map<String, dynamic> dummyCategories = {
  "categories": [
    {
      "id": 2,
      "name": "Headphones",
      "image": {
        "id": 8,
        "file_name": "asset/images/fausto-sandoval-w5m3PIGvkqI-unsplash.jpg",
      }
    },
    {
      "id": 3,
      "name": "Mobiles",
      "image": {
        "id": 9,
        "file_name": "asset/images/adds3.jpg",
      }
    },
  ],
};
