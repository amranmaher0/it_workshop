import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BottomSheets {
  // ~ main Function : we call Get.bottomSheet() one at a time only
  static void show(
    Widget widget, {
    double? bottomSheetHeight,
    double? bottomSheetWidth,
    Color? bottomSheetColor,
    Color? barrierColor,
    double? blurShadow,
    double? elevation,
    bool isDismissible = true,
    Decoration? bottomSheetDecoration,
    ShapeBorder? shapeBorder,
  }) {
    Get.bottomSheet(
      Container(
        height: bottomSheetHeight ?? Get.height / 2,
        width: bottomSheetWidth ?? Get.width,
        decoration: const BoxDecoration(),
        child: Container(
          decoration: bottomSheetDecoration ??
              BoxDecoration(
                color: bottomSheetColor ?? Colors.white,
                boxShadow: [
                  BoxShadow(
                    blurRadius: blurShadow ?? 20.0,
                  ),
                ],
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
              ),
          child: widget,
        ),
      ),
      isDismissible: isDismissible,
      barrierColor: barrierColor ?? Colors.transparent,
      elevation: elevation ?? 30,
      backgroundColor: bottomSheetColor ?? Colors.white,
      shape: shapeBorder ??
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
    );
  }

  static void showBlaBla() {
    show(
      Container(),
    );
  }

  static void showBlaBlaToo() {
    show(
      Container(),
      bottomSheetHeight: Get.height / 3,
    );
  }
}
