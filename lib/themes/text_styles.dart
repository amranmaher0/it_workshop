import 'package:flutter/material.dart';

import '../themes/app_colors.dart';

TextStyle customTextStyle({
  bool isBold = false,
  bool ellipsis = false,
  required Color? color,
  required double? fontSize,
}) =>
    TextStyle(
      overflow: ellipsis ? TextOverflow.ellipsis : null,
      fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
      color: color ?? AppColors.primaryColor,
      fontSize: fontSize,
    );
TextStyle h1TextStyle(
        {bool isBold = false, bool ellipsis = false, required Color? color}) =>
    TextStyle(
      overflow: ellipsis ? TextOverflow.ellipsis : null,
      fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
      color: color ?? AppColors.primaryColor,
      fontSize: 18,
    );

TextStyle h2TextStyle(
        {bool isBold = false, bool ellipsis = false, required Color? color}) =>
    TextStyle(
      overflow: ellipsis ? TextOverflow.ellipsis : null,
      fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
      color: color ?? AppColors.primaryColor,
      fontSize: 18,
    );

TextStyle h3TextStyle(
        {bool isBold = false, bool ellipsis = false, required Color? color}) =>
    TextStyle(
      overflow: ellipsis ? TextOverflow.ellipsis : null,
      fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
      color: color ?? AppColors.primaryColor,
      fontSize: 15,
    );

TextStyle h4TextStyle(
        {bool isBold = false, bool ellipsis = false, required Color? color}) =>
    TextStyle(
      overflow: ellipsis ? TextOverflow.ellipsis : null,
      fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
      color: color ?? AppColors.primaryColor,
      fontSize: 12,
    );
TextStyle h5TextStyle(
        {bool isBold = false, bool ellipsis = false, required Color? color}) =>
    TextStyle(
      overflow: ellipsis ? TextOverflow.ellipsis : null,
      fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
      color: color ?? AppColors.primaryColor,
      fontSize: 8,
    );
