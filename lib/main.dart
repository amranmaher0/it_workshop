import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:it_workshop/core/routing/routing_manager.dart';
import 'package:it_workshop/helper/dependency_initialiazier.dart';

void main() {
  DependencyInitializer.dependencyInjector();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: RoutesName.splashScreen,
      getPages: RoutingManager.pages,
    );
  }
}
