import 'package:flutter/material.dart';
import 'package:it_workshop/common/widgets/dm_logo_image.dart';
import 'package:it_workshop/core/routing/routing_manager.dart';
import 'package:it_workshop/themes/app_colors.dart';
import 'package:it_workshop/extensions/widget_extension.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Future.delayed(
      const Duration(seconds: 1),
      () {
        RoutingManager.offNamed(RoutesName.registerScreen);
      },
    );
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const DmLogoWidget().center().expanded(flex: 1),
        ],
      ),
    );
  }
}
