import 'package:it_workshop/common/constants.dart';
import 'package:it_workshop/common/dummy_data.dart';
import 'package:it_workshop/features/home/data/models/category_model.dart';
import 'package:it_workshop/features/home/data/sources/base.dart';

class DummyHomeSource extends BaseHomeSource {
  @override
  Future<List<CategoryModel>> getCategories() async {
    await Future.delayed(const Duration(seconds: delayTime));
    List<CategoryModel> result = CategoryModel.fromJsonList(dummyCategories);
    return result;
  }
}
