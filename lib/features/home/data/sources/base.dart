import 'package:it_workshop/features/home/data/models/category_model.dart';

abstract class BaseHomeSource {
 Future<List<CategoryModel>> getCategories();
}
