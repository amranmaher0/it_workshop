import 'package:it_workshop/common/end_points.dart';
import 'package:it_workshop/common/enums.dart';
import 'package:it_workshop/core/network/http.dart';
import 'package:it_workshop/features/home/data/models/category_model.dart';
import 'package:it_workshop/features/home/data/sources/base.dart';

class RemoteHomeSource extends BaseHomeSource {
 @override
 Future<List<CategoryModel>> getCategories() async {
 Request request = Request(
 EndPoints.getCategory,
 RequestType.get,
 );
 Map<String, dynamic> jsonString = await request.sendRequest();
 return CategoryModel.fromJsonList(jsonString);
 }
}
