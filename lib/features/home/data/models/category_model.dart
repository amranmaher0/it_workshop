class CategoryModel {
 CategoryModel({
 required this.id,
 required this.name,
 required this.image,
 });

 int id;
 String name;
 String image;

 factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
 id: json["id"],
 name: json["name"],
 image: json["image"]["file_name"],
 );

 static List<CategoryModel> fromJsonList(Map<String, dynamic> json) {
 List<CategoryModel> list = [];
 list = List<CategoryModel>.from(
 json["categories"].map(
 (x) => CategoryModel.fromJson(x),
 ),
 );
 return list;
 }
}
