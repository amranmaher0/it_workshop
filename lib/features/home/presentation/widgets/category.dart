import 'package:flutter/material.dart';
import 'package:it_workshop/features/home/data/models/category_model.dart';
import 'package:it_workshop/themes/app_colors.dart';

class CategoryWidget extends StatelessWidget {
  final CategoryModel category;
  CategoryWidget({
    required this.category,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.primaryColor, width: 1),
        borderRadius: BorderRadius.circular(10),
        // ~ if you are using API =>> use NetworkImage
        image: DecorationImage(
          image: AssetImage(category.image),
          fit: BoxFit.cover,
        ),
      ),
      margin: const EdgeInsets.only(top: 10),
      width: MediaQuery.of(context).size.width,
      height: 150,
      child: Text(
        category.name,
        style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 22,
            shadows: [
              Shadow(blurRadius: 7, color: Colors.black, offset: Offset(1, 3))
            ]),
      ),
    );
  }
}
