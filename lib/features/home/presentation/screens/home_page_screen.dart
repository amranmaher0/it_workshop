import 'package:flutter/material.dart';
import 'package:it_workshop/common/widgets/dm_app_bar_widget.dart';
import 'package:it_workshop/features/home/buisness_logic/home_controller.dart';
import 'package:it_workshop/features/home/data/models/category_model.dart';
import 'package:it_workshop/features/home/presentation/widgets/category.dart';
import 'package:get/get.dart';
import 'package:it_workshop/helper/rx_viewr.dart';

class HomePageScreen extends StatefulWidget {
  HomePageScreen({Key? key}) : super(key: key);

  @override
  State<HomePageScreen> createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  final HomeController homeController = Get.find<HomeController>();
  @override
  void initState() {
    // ~ WidgetsBinding.instance.addPostFrameCallback :
    //  call after build() method done
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await homeController.getCategories();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DmAppBarWidget(
        title: 'Category',
      ),
      body: Obx(
        () => RxViewer(
          rxFuture: homeController.categories,
          child: ListView.builder(
            itemCount: homeController.categoriesCount,
            itemBuilder: (BuildContext context, index) {
              CategoryModel category = homeController.categories.result[index];
              return CategoryWidget(
                category: category,
              );
            },
          ),
        ),
      ),
    );
  }
}
