import 'dart:developer';

import 'package:get/get.dart';
import 'package:rx_future/rx_future.dart';
import 'package:it_workshop/features/home/data/models/category_model.dart';
import 'package:it_workshop/features/home/data/sources/dummy.dart';
import 'package:it_workshop/features/home/data/sources/remote.dart';

class HomeController extends GetxController {
// -----------------data sources-----------------------

  final RemoteHomeSource _remoteSource = RemoteHomeSource();
  final DummyHomeSource _dummySource = DummyHomeSource();

// -----------------Response Models-----------------------

  final RxFuture<List<CategoryModel>> categories = RxFuture([]);

// -----------------Getters-----------------------

  int get categoriesCount => categories.result.length;

// -----------------Methods-----------------------

  Future<void> getCategories() async {
    await categories.observe(
      (_) async {
        return await _dummySource.getCategories();
      },
      onSuccess: (p0) {
        log('Success');
      },
    );
  }
}
