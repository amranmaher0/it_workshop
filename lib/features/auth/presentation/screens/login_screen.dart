import 'package:flutter/material.dart';
import 'package:it_workshop/common/widgets/dm_button_widget.dart';
import 'package:it_workshop/common/widgets/dm_logo_image.dart';
import 'package:it_workshop/common/widgets/dm_textfield_widget.dart';
import 'package:it_workshop/helper/rx_viewr.dart';
import 'package:it_workshop/features/auth/business_logic/auth_controller.dart';
import 'package:get/get.dart';
import 'package:it_workshop/themes/app_colors.dart';
import 'package:it_workshop/themes/text_styles.dart';
import '../../../../core/routing/routing_manager.dart';
import 'package:it_workshop/extensions/widget_extension.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const DmLogoWidget(
              color: AppColors.primaryColor,
            ).center().padding(const EdgeInsets.all(8)),
            Text(
              "DM mobile",
              style: h3TextStyle(color: AppColors.primaryColor, isBold: true),
            ).center().padding(
                  const EdgeInsets.symmetric(vertical: 8),
                ),
            Text(
              "Welcome to DM mobile",
              style: h3TextStyle(color: AppColors.primaryColor, isBold: true),
            ).center().padding(
                  const EdgeInsets.symmetric(vertical: 8),
                ),
            DmTextFieldWidget(
                    borderColor: AppColors.primaryColor,
                    labelText: 'Email',
                    onChange: (value) {},
                    textFieldType: TextInputType.emailAddress)
                .padding(
              const EdgeInsets.only(top: 3, bottom: 10),
            ),
            DmTextFieldWidget(
                    borderColor: AppColors.primaryColor,
                    labelText: 'Password',
                    onChange: (value) {},
                    textFieldType: TextInputType.emailAddress)
                .padding(
              const EdgeInsets.only(top: 3, bottom: 10),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Back Button
                DmButtonWidget(
                  borderColor: AppColors.primaryColor,
                  onTap: () {
                    RoutingManager.offAllNamed(RoutesName.registerScreen);
                  },
                  title: 'Back',
                ).paddingSymmetric(horizontal: 8).expanded(flex: 1),
                // Login Button
                DmButtonWidget(
                  buttonColor: AppColors.primaryColor,
                  onTap: () async {
                    RoutingManager.offAllNamed(RoutesName.homePage);
                  },
                  title: 'Login',
                ).paddingSymmetric(horizontal: 8).expanded(flex: 1),
              ],
            ).paddingSymmetric(vertical: 16),
            const Padding(padding: EdgeInsets.only(top: 10)),
            DmButtonWidget(
              buttonColor: AppColors.primaryColor,
              borderRadius: BorderRadius.circular(5),
              textColor: Colors.white,
              onTap: () {},
              title: 'Skip',
            ).center(),
          ],
        ).padding(
          const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
        ),
      ),
    );
  }
}
