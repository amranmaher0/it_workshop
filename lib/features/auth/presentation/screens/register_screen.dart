import 'package:flutter/material.dart';
import 'package:it_workshop/common/widgets/dm_button_widget.dart';
import 'package:it_workshop/common/widgets/dm_logo_image.dart';
import 'package:it_workshop/common/widgets/dm_textfield_widget.dart';
import 'package:get/get.dart';
import 'package:it_workshop/core/routing/routing_manager.dart';
import 'package:it_workshop/features/auth/business_logic/auth_controller.dart';
import 'package:it_workshop/extensions/widget_extension.dart';
import 'package:it_workshop/themes/app_colors.dart';
import 'package:it_workshop/themes/text_styles.dart';

class RegistrationScreen extends StatelessWidget {
  RegistrationScreen({super.key});
  AuthController authController = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const DmLogoWidget(
                color: AppColors.primaryColor,
              ).center().padding(const EdgeInsets.all(8)),
              Text(
                "DM mobile",
                style: h3TextStyle(
                  color: AppColors.primaryColor,
                  isBold: true,
                ),
              ).center().padding(
                    const EdgeInsets.symmetric(vertical: 8),
                  ),
              Text(
                'Welcome to DM mobile',
                style: h3TextStyle(
                  color: AppColors.primaryColor,
                  isBold: true,
                ),
              ).center(),
              DmTextFieldWidget(
                borderColor: AppColors.primaryColor,
                textFieldType: TextInputType.name,
                labelText: 'Name',
                onChange: (value) {},
              ).padding(
                const EdgeInsets.only(top: 3, bottom: 10),
              ),
              DmTextFieldWidget(
                borderColor: AppColors.primaryColor,
                textFieldType: TextInputType.emailAddress,
                labelText: 'Email',
                onChange: (value) {},
              ).padding(
                const EdgeInsets.only(top: 3, bottom: 10),
              ),
              DmTextFieldWidget(
                      borderColor: AppColors.primaryColor,
                      textFieldType: TextInputType.visiblePassword,
                      labelText: 'Password',
                      onChange: (value) {})
                  .padding(
                const EdgeInsets.only(top: 3, bottom: 10),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  DmButtonWidget(
                    borderColor: AppColors.primaryColor,
                    onTap: () {
                      RoutingManager.offAllNamed(RoutesName.homePage);
                    },
                    title: 'Sign Up',
                  ).paddingSymmetric(horizontal: 8).expanded(flex: 1),
                  DmButtonWidget(
                    buttonColor: AppColors.primaryColor,
                    onTap: () async {
                      RoutingManager.offAllNamed(RoutesName.loginScreen);
                    },
                    title: 'Login Instead',
                  ).paddingSymmetric(horizontal: 8).expanded(flex: 1),
                ],
              ).paddingSymmetric(vertical: 18),
            ],
          ),
        ),
      ),
    );
  }
}
