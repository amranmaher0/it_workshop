import 'package:get/get.dart';
import 'package:rx_future/rx_future.dart';
import 'package:it_workshop/common/models/success_model.dart';
import 'package:it_workshop/core/local_storage/local_storage.dart';
import 'package:it_workshop/features/auth/data/models/register_model.dart';
import 'package:it_workshop/features/auth/data/services/remote.dart';

import '../data/models/login_model.dart';

class AuthController extends GetxController {
  // -----------------data sources-----------------------
  LocalStorage storage = LocalStorage();
  AuthRemoteSource remoteAuth = AuthRemoteSource();

  // -----------------request models-----------------------
  final Rx<LoginRequestModel> loginRequestModel = Rx(LoginRequestModel.zero());
  final Rx<RegisterRequestModel> registerRequestModel =
      Rx(RegisterRequestModel.zero());

  // -----------------response models-----------------------
  RxFuture<StatusModel> registerState = RxFuture(StatusModel.zero());
  RxFuture<LoginResponseModel> loginState = RxFuture(LoginResponseModel.zero());

  // ----------------- methods -----------------------

  Future<void> register() async {

    // await registerState.observe(
    //   (_) async {
    //     return await remoteAuth.register(registerRequestModel.value);
    //   },
    // );

  }

  Future<void> login() async {

    // await loginState.observe(
    //   (_) async {
    //     return await remoteAuth.login(loginRequestModel.value);
    //   },
    //   onSuccess: (val) {
    //     storage.saveToken(val.token);
    //   },
    // );

  }
}
