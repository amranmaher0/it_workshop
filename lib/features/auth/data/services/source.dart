import 'package:it_workshop/common/models/success_model.dart';
import 'package:it_workshop/features/auth/data/models/login_model.dart';
import 'package:it_workshop/features/auth/data/models/register_model.dart';

abstract class AuthBaseSource {
 Future<LoginResponseModel> login(LoginRequestModel loginRequestModel);
 Future<StatusModel> register(RegisterRequestModel registerRequestModel);
}
