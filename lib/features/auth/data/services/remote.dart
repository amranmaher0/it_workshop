import 'package:it_workshop/common/end_points.dart';
import 'package:it_workshop/common/enums.dart';
import 'package:it_workshop/common/models/success_model.dart';
import 'package:it_workshop/core/network/http.dart';
import 'package:it_workshop/features/auth/data/models/register_model.dart';
import 'package:it_workshop/features/auth/data/models/login_model.dart';
import 'package:it_workshop/features/auth/data/services/source.dart';

class AuthRemoteSource extends AuthBaseSource {
  @override
  Future<LoginResponseModel> login(LoginRequestModel loginRequestModel) async {
    Request request = Request(
      EndPoints.login,
      RequestType.post,
      isFormData: true,
      body: loginRequestModel.toJson(),
    );
    Map<String, dynamic> jsonString = await request.sendRequest();
    return LoginResponseModel.fromJson(jsonString);
  }

  @override
  Future<StatusModel> register(
      RegisterRequestModel registerRequestModel) async {
    Request request = Request(
      EndPoints.register,
      RequestType.post,
      isFormData: true,
      body: registerRequestModel.toJson(),
    );
    Map<String, dynamic> jsonString = await request.sendRequest();
    return StatusModel.fromJson(jsonString);
  }
}
