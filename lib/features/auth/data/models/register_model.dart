
class RegisterRequestModel {
  String email;
  String password;
  String confirmPassword;
  String name;

  RegisterRequestModel({
    required this.email,
    required this.password,
    required this.confirmPassword,
    required this.name,
  });

  factory RegisterRequestModel.zero() => RegisterRequestModel(
        email: '',
        password: '',
        confirmPassword: '',
        name: '',
      );

  Map<String, dynamic> toJson() {
    return {
      "email": email,
      "password": password,
      "name": name,
    };
  }

  void clear() {
    email = "";
    password = "";
    name = "";
  }
}
