// ~  request model

class LoginRequestModel {
  String email;
  String password;

  LoginRequestModel({
    required this.email,
    required this.password,
  });
  // ~ this Constructor to assign initial value
  factory LoginRequestModel.zero() => LoginRequestModel(
        email: "",
        password: "",
      );
  // ~ toJson mothod is use for sending this model in body request
  Map<String, dynamic> toJson() {
    return {
      "email": email,
      "password": password,
    };
  }

  void clear() {
    password = "";
    email = "";
  }
}

// ~  response model

class LoginResponseModel {
  LoginResponseModel({
    required this.status,
    required this.token,
  });

  String status;
  String token;

  factory LoginResponseModel.zero() => LoginResponseModel(
        token: '',
        status: '',
      );

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) =>
      LoginResponseModel(
        status: json["status"],
        token: json["token"],
      );
}
