import 'package:get/instance_manager.dart';
import 'package:it_workshop/features/auth/business_logic/auth_controller.dart';
import 'package:it_workshop/features/home/buisness_logic/home_controller.dart';

class DependencyInitializer {
  static dependencyInjector() {
    Get.put<AuthController>(AuthController());
    Get.put<HomeController>(HomeController());
  }
}
