import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:rx_future/rx_future.dart';
import 'package:it_workshop/extensions/widget_extension.dart';

class RxViewer extends StatelessWidget {
  const RxViewer({
    Key? key,
    required this.rxFuture,
    required this.child,
  }) : super(key: key);
  final RxFuture rxFuture;
  final Widget child;
  @override
  Widget build(BuildContext context) {
    if (rxFuture.loading) {
      return const CircularProgressIndicator(
        color: Colors.red,
      ).center();
    } else if (rxFuture.hasError) {
      return child;
    } else {
      return child;
    }
  }
}
