import 'package:get/get.dart';
import 'package:it_workshop/features/auth/presentation/screens/login_screen.dart';
import 'package:it_workshop/features/auth/presentation/screens/register_screen.dart';
import 'package:it_workshop/features/home/presentation/screens/home_page_screen.dart';
import 'package:it_workshop/features/splash_screen/splash_screen.dart';

class RoutesName {
  static String splashScreen = '/splash_screen';
  static String registerScreen = '/register_screen';
  static String loginScreen = '/login_screen';
  static String homePage = '/home_page';
}

class RoutingManager {
  static List<GetPage<dynamic>> pages = [
    GetPage(name: RoutesName.splashScreen, page: () => const SplashScreen()),
    GetPage(name: RoutesName.registerScreen, page: () => RegistrationScreen()),
    GetPage(name: RoutesName.loginScreen, page: () => LoginScreen()),
    GetPage(name: RoutesName.homePage, page: () => HomePageScreen())
  ];
  // push screen to stack without replacement
  static void toNamed(String route) {
    Get.toNamed(route);
  }
  // push screen to stack with replacement 
  static void offNamed(String route) {  
    Get.offNamed(route);
  }
  // clear stack and push screen to it 
  static void offAllNamed(String route) {
    Get.offAllNamed(route);
  }
}
