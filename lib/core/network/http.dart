import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:it_workshop/common/custom_exceptions.dart';
import 'package:it_workshop/common/enums.dart';
import 'package:it_workshop/core/local_storage/local_storage.dart';
import 'package:it_workshop/core/network/dio.dart';
import 'package:it_workshop/utils/utils.dart';

class Request {
  String endPoint;
  bool authorized;
  bool isFormData;
  RequestType method;
  Map<String, dynamic>? headers;
  Map<String, dynamic>? body;

  Request(
    this.endPoint,
    this.method, {
    this.authorized = false,
    this.isFormData = false,
    this.headers,
    this.body,
  }) {
    if (authorized) {
      headers = {};
      LocalStorage storage = LocalStorage();
      headers!["authorization"] = "Bearer " + storage.getToken();
    }

    if (isFormData) {
      log('im form data');
      FormData f = FormData.fromMap(body!);
    }
  }

  Future<Map<String, dynamic>> sendRequest() async {
    Response? response;
    Map<String, dynamic> headers = {
      'content_Type': 'application/json',
    };
    try {
      response = await DioInstance().dio.request(
            endPoint,
            data: isFormData ? FormData.fromMap(body!) : body,
            options: Options(
              method: Utils.requestTypeToString(method),
              headers: headers,
              contentType: 'application/json',
            ),
          );

      if (response.statusCode! >= 200 && response.statusCode! < 300) {
        //TODO: this should be handled in diffren way.
        if (response.data is String) return json.decode(response.data);
        return response.data;
      }
    } on DioError catch (error) {
      // handling http status code exceptions
      if (error.type == DioErrorType.response) {
        // handling bad requests.
        if (error.response!.statusCode == 400) {
          // this line is really depends on what server responds, and how it reply with errors.
          throw badRequestException[error.response!.data["message"]] ??
              GenericException(
                type: ExceptionType.Other,
              );
        }

        // handling other status codes.
        throw statusCodesException[error.response!.statusCode] ??
            GenericException(
              type: ExceptionType.Other,
            );
      }

      // handling connection problems.
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.sendTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.other) {
        throw GenericException(
          type: ExceptionType.ConnectionError,
          errorMessage: "You Have no Internet Connection",
        );
      }
    }
    return {};
  }
}
