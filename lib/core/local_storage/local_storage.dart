import 'package:shared_preferences/shared_preferences.dart';
import 'package:it_workshop/common/constants.dart';

class LocalStorage {
  late SharedPreferences prefs;
  Future<void> getInstance() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<void> saveToken(String token) async {
    await prefs.setString(TOKEN, token);
  }

  String getToken() {
    return prefs.getString(TOKEN) ?? '';
  }
}
